# -*- coding: utf-8 -*-
import requests
from bs4 import BeautifulSoup
import os
import json 
import urllib
import re
import glob
from datetime import datetime
def crawler():
    num_page = 0
    date = '[0-9]{2}/June/2016'
    news = dict
    url = "https://www.blognone.com"
    months = ['January','February','March','April','May','June','July','August','September','October','November','December']
    #get data from web
    #for num_page in range(10):
    while(True):
        # check page
        if num_page==0: 
            #response = requests.get(url)
            response = urllib.urlopen(url)
        else:
            #response = requests.get(url_page)
            response = urllib.urlopen(next_page)
    
        soup = BeautifulSoup(response.read(),"html.parser")
        #get url next page
        urlpage = soup.find_all('li',{'class':'pager-next'})[0].find_all('a')[0].get('href')
        next_page = url+urlpage
        content_store =  soup.find_all('div',{'class':'content-title-box'})
        
        '''
        regex = '<div class="field-item (.+)">(.+)</div>'
        pt = re.compile(regex)
        tag = re.findall(pt,soup)
        print tag


        '''

        
        try:
            for content in content_store:
                #print content
                for link in content.find_all('a'):
                            
                    #get link content
                    link_content = link.get('href')
                    
                    url_link = url+str(link_content)
                    response_link = urllib.urlopen(url_link)
                    soup2 = BeautifulSoup(response_link.read(),'html.parser')
                    data_store = soup2.find_all("div",{"class":"content-container"})
                    b_by = ''    
                    for data in data_store:

                        #get head news
                        head = data.find_all('h2')[0].text
                        #print d.find_all('div',{'class':'content-title-box'})[0].text
                
                        #get date-time
                        meta_date = data.find_all("span",{"class":"submitted"})
                        meta_date =  meta_date[0].text.split(" ") 
                        by = meta_date[1]
                        if meta_date[2]!= '':
                            by+=' '+meta_date[2]
                        
                        date = meta_date[len(meta_date)-5]+"/"+meta_date[len(meta_date)-4]+"/"+meta_date[len(meta_date)-3]
                        time = meta_date[len(meta_date)-1]
                        month = meta_date[len(meta_date)-4]

                        #check month
                        if months.index(month)+1<4 and meta_date[len(meta_date)-3] == '2016':
                            return
                        
                                   
                        #get tags
                        d = str(data)
                        tags = []
                        tags_data = data.find_all("div",{"class":"field-items"})[0].find_all("div",{"class":"field-item"})                        
                        for tag in tags_data:
                            if re.match(r'[a-zA-Z]',tag.text):
                                tags.append(tag.text)
                                #print tag.text
                        if tags ==[]:
                            tags = "-"

                        #get content
                        contents = data.find_all_next("div",{"class":"node-content"})[0].text
                        contents =  contents.split("\n")
                        content_new = ""
                        #remove newline
                        for c in contents:
                            content_new+=c+" "
                       
                        #get image link
                        image = data.find_all('div',{'class':'content'})[0].find_all('div',{'class':'content'})
                        #print image
                        image_link = []
                        image_list = []
                        #get all image link
                        image_list = [i['src'] for img in image for i in img.find_all("img")]
                        for img in image_list:
                            path = "https://www.blognone.com/sites/default/files/badges/"
                            if img.startswith(path):
                                break
                            image_link.append(img)
                        if image_link ==[]:
                            image_link = "-"
                        
                        #print contents
                
                    #add data to dict
                    news = {'News':head,'By':by,'Date':date,'Time':time,'Tags':tags,'Contents':content_new,'Link':url_link,'Image-Link':image_link}

                    #set output path
                    setpath = date.replace("/",'-')

                    #set filename
                    setfilename = time.replace(":","-")
                    setfilename = setfilename+'_'+setpath

                    #check filename
                    if checkfile(setfilename)==True:
                        path = "C:\\xampp\htdocs\project\output_news\output_json\\"
                        filename = os.path.join(path,setfilename+'.json')
                        i = 0
                        #check filename is same news
                        with open(filename) as json_file:
                            #print json_file.read()
                            js_data =   json.loads(json_file.read().decode('utf-8'))
                            i+=1
                        #get by 
                        b_by = js_data['By']
               
                        if b_by == by:
                            if num_page>=3:
                                print num_page
                                print filename
                                return
                            break
                        else:
                            #set new filename
                            if setfilename[-2]=='_':
                                i = int(setfilename[-1])+1
                            setfilename +='_'+str(i) 
                        
                    #write jsonfile
                    writejson(news,setfilename)
                    '''
                    #write text file
                    writetext(news,setfilename)
                    '''
                    #write data
                    #writedata(news,setpath,setfilename)
            num_page+=1
                
        except ValueError as e:
                 print e.value

#check file name
def checkfile(filename):
    path = "C:\\xampp\htdocs\project\output_news\output_json\\"
    if checkpath(path)==True:
        return False
    else:
        flist=glob.glob(r'C:\\xampp\htdocs\project\output_news\output_json\*.json')
        for fname in flist:
            name = os.path.basename(fname).replace('.json','')
            if name == filename:
                return True
    return False


                 
#write data to json file                 
def writejson(news,filename):
    try:
        path = "C:\\xampp\htdocs\project\output_news\output_json\\" 
        if checkpath(path)==True:
            os.makedirs(path)
        filename = os.path.join(path,filename+'.json')
        if checkpath(filename)==True:
            with open(filename,'w') as write_json:
                write_json.write(json.dumps(news,ensure_ascii = False,sort_keys=True,skipkeys=True,indent=2).encode('utf8'))
            
            '''
            write_data = open(filename,'w')
            json_string = json.dumps(news,ensure_ascii = False,sort_keys=True,skipkeys=True,indent=2).encode('utf8')
            #json_string = json_string.decode('string_escape')
            #print type(json_string)
            write_data.write(json_string)
            write_data.close()
            '''
    except ValueError as e:
        print e.value
'''        
#write data to text file
def writetext(news,filename):
     try:
         #set output path
         path = "output_news19-6-59/output_text/" 
         #Check path
         if checkpath(path)==True:
             os.makedirs(path)
         #set filename
         filename = os.path.join(path,filename+'.txt')
         if checkpath(filename)==True:
             #write data
             write_data = open(filename,'w')
             new = ""
             new = news['News'].encode("UTF-8")+news['Contents'].encode("UTF-8")
             write_data.write(new)
             write_data.close()
     except ValueError as e:
         print e.value
'''

#Check path file is have         
def checkpath(path):
    if not os.path.exists(path):
        return True
    return False
    
def main():
    print "Start Crawler\n"
    crawler()
    end_time = datetime.now()
    print'Duration: ',format(end_time - start_time)
    
start_time = datetime.now()
if __name__ == "__main__":
    main()
                      
