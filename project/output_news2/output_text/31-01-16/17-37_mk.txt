https://www.blognone.com/node/7730317:3731/01/16โอบามาประกาศโครงการ Computer Science For All นักเรียนทุกคนต้องได้หัดเขียนโค้ดhttps://www.blognone.com/sites/default/files/imagecache/news-thumbnail/category_pictures/whitehouse.pngComputer Science
Obama
Whitehouse
mk
ประธานาธิบดีบารัค โอบามา แห่งสหรัฐอเมริกา ประกาศโครงการ Computer Science For All ส่งเสริมให้นักเรียนชาวอเมริกันทุกคนมีโอกาสเรียนวิชาวิทยาการคอมพิวเตอร์ เพื่อฝึกทักษะการเป็น "ผู้สร้าง" ในระบบเศรษฐกิจดิจิทัล แทนการเป็นผู้บริโภคแต่เพียงฝ่ายเดียว
โอบามาบอกว่า "วิทยาการคอมพิวเตอร์" กลายเป็นทักษะพื้นฐานที่ทุกคนต้องมีไปเรียบร้อยแล้ว รัฐบาลสหรัฐจะตั้งงบประมาณ 4 พันล้านดอลลาร์เพื่อพัฒนาการสอนวิชาคอมพิวเตอร์ในโรงเรียน และให้งบประมาณสนับสนุน National Science Foundation (NSF) พัฒนาหลักสูตรและฝึกอบรมครูสอนวิชาวิทยาการคอมพิวเตอร์เพิ่มด้วย
โครงการนี้ของรัฐบาลสหรัฐยังได้รับเสียงสนับสนุนอย่างล้นหลามจากวงการไอที โดยคนดังของวงการอย่าง Bill Gates, Mark Zuckerberg, Jack Dorsey ออกมาโพสต์ชื่นชม และบริษัทใหญ่ทั้ง Apple, Microsoft, Google, Facebook, Salesforce, Qualcomm ก็เข้าร่วมโครงการด้วย
ที่มา - Whitehouse Blog, Whitehouse Press


.@codeorg is making an extraordinary commitment to prepare 25,000 teachers to teach computer science: https://t.co/8ipynh06D4 #CSForAll
— Bill Gates (@BillGates) January 30, 2016
Thank you @POTUS and @codeorg for pushing all schools to teach computer science. https://t.co/IJERCgQBev #CSforAll
— Jack (@jack) January 30, 2016
