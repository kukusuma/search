https://www.blognone.com/node/7727322:3229/01/16พบช่องโหว่ความปลอดภัยใน LG G3, ทาง LG ออกแพตช์แก้แล้วhttps://www.blognone.com/sites/default/files/imagecache/news-thumbnail/category_pictures/g3.pngLG
LG G3
Security
mk
ผู้เชี่ยวชาญความปลอดภัยค้นพบช่องโหว่ของ LG G3 มือถือเรือธงของ LG ในปี 2014 สาเหตุเกิดจากแอพชื่อ Smart Notice ที่มาพร้อมกับ LG G3 ทุกเครื่อง
Smart Notice ใช้ WebView เพื่อเรียกข้อมูลเว็บเพจจากเซิร์ฟเวอร์ แต่ไม่ตรวจสอบข้อมูล (payload) ที่ได้รับมาว่าปลอดภัยหรือไม่ แฮ็กเกอร์อาจใช้ช่องโหว่นี้ส่งโค้ด JavaScript จากเซิร์ฟเวอร์มารันใน WebView ได้
นักวิจัยส่งข้อมูลนี้ให้ LG แล้ว และ LG ก็ออกแพตช์เรียบร้อยแล้ว ใครที่มี G3 อยู่ก็ลองตรวจสอบว่ามีอัพเดตหรือไม่ครับ
ที่มา - Ars Technica

