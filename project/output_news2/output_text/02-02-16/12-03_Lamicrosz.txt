https://www.blognone.com/node/7738512:0302/02/16Funker เปิดตัว W5.5 Pro โทรศัพท์ระบบปฏิบัติการ Windows 10 Mobilehttps://www.blognone.com/sites/default/files/imagecache/news-thumbnail/category_pictures/w10m.pngMobile
Windows 10 Mobile
Lamicrosz
Funker ผู้ผลิตโทรศัพท์สัญชาติสเปนเปิดตัว W5.5 Pro โทรศัพท์ระบบปฏิบัติการ Windows 10 Mobile มาพร้อมสเปค ดังนี้

หน้าจอ: 5.5 นิ้ว ความละเอียด 1280x720 พิกเซล
หน่วยประมวลผล: Qualcomm Snapdragon 410 ควอดคอร์ ความถี่ 1.2 GHz
แรม: 2 GB
พื้นที่เก็บข้อมูล: 16 GB
แบตเตอรี่: 2600 mAh
กล้องหลัง: 13 ล้านพิกเซล แฟลช 2 สี
กล้องหน้า: 5 ล้านพิกเซล
น้ำหนัก: 135 กรัม

Funker เปิดราคา W5.5 Pro มาที่ 239 ยูโร (ประมาณ 9,300 บาท)
ที่มา - Funker (ภาษาสเปน) via Windows Central

