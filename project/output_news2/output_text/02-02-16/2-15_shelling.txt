https://www.blognone.com/node/773622:1502/02/16Barracuda ประกาศปิดบริการคลาวด์ Copy และ CudaDrive 1 พ.ค. นี้ https://www.blognone.com/sites/default/files/imagecache/news-thumbnail/news-thumbnails/logo_horizontal_barracuda.pngCloud Storage
Copy
shelling
ถึงเวลาหาที่ฝากไฟล์ใหม่กันอีกครั้ง เมื่อ Barracuda ผู้ให้บริการด้านความปลอดภัยบนเครือข่าย ประกาศปิดตัวบริการ Copy และ CudaDrive บริการกลุ่มเมฆของบริษัทในวันที่ 1 พฤษภาคมนี้ โดยทาง Barracuda ให้เหตุผลว่าต้องการนำทรัพยากรไปทุ่มให้กับบริการตัวอื่น โดยหลังวันที่ 1 พฤษภาคม ไฟล์ทั้งหมดรวมถึงชื่อบัญชีของผู้ใช้จะไม่สามารถเข้าถึงได้อีก
ในระหว่างนี้ ผู้ใช้ของ Copy และ CudaDrive ยังสามารถใช้งานได้ตามปกติ และสามารถดาวน์โหลดไฟล์ทั้งหมดลงมาไว้ในเครื่อง หรือจะโอนถ่ายไฟล์ไปเก็บไว้ที่กลุ่มเมฆเจ้าอื่นผ่านบริการชื่อ Mover ก็ได้ โดยทาง Barracuda ได้เตรียมหน้าคำแนะนำไว้ให้แล้ว
ที่มา - Barracuda (Announcement, Blog)
