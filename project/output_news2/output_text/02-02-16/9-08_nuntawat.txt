https://www.blognone.com/node/773679:0802/02/16ไมโครซอฟท์ปล่อย Windows 10 Mobile Insider Preview Build 10586.71https://www.blognone.com/sites/default/files/imagecache/news-thumbnail/category_pictures/w10m.pngMicrosoft
Operating System
Windows 10 Mobile
nuntawat
ไมโครซอฟท์ปล่อย Windows 10 Mobile Insider Preview Build 10586.71 ซึ่งเป็น Cumulative Update ให้สมาชิก Windows Insider กลุ่ม Fast Ring แล้ว
ไมโครซอฟท์กล่าวว่า หากผู้ใช้ติดตั้ง Configuration Update เมื่อวันศุกร์ที่ผ่านมา จะพบว่าการติดตั้ง Cumulative Update เสถียรขึ้น ทั้งนี้เว็บไซต์ MicrosoftInsider.es กล่าวว่า Configuration Update ทำให้การอัพเดตเป็นการดาวน์โหลดเฉพาะไฟล์คล้ายกับการอัพเดตบนพีซี ไม่ใช่เป็นการดาวน์โหลด ROM ใหม่
สำหรับรายละเอียดของอัพเดตมีดังนี้

ปรับปรุงประสิทธิภาพและเสถียรภาพของ Windows Update
ปรับปรุงการโยกย้าย (migration) การตั้งค่าเครือข่ายและข้อความ (messaging) เมื่ออัพเกรดจาก WP8.1
API ของ SensorCore ได้รับการอัพเกรดอย่างถูกต้องจาก WP8.1 แล้ว ทำให้แอพที่ใช้งาน API ดึงข้อมูลได้ถูกต้อง
ปรับปรุงการตรวจจับ SD card และ File Explorer สามารถจัดการกับการ์ดเมื่อถูกถอดออกได้แล้ว
ปรับปรุง Microsoft Edge รวมถึงการแสดงผลไฟล์ PDF
ปรับปรุงการเชื่อมต่อ Bluetooth อีกครั้ง (re-connecting) กับอุปกรณ์ที่ถูกจับคู่ไว้แล้ว
ปรับปรุง Settings เมื่อดาวน์โหลดแผนที่เพื่อการใช้งานแบบออฟไลน์และการตั้งค่า Quick Settings
แก้บั๊กเล่นเพลงที่ติด DRM หากติดตั้งแอพ Groove Music ใหม่
ปรับปรุงประสิทธิภาพการใช้พลังงานหลังเล่นเพลง หลังพลาดการรับสายกรณีเปิดการตรวจจับม่านตาไว้ และหลังดาวน์โหลดอัพเดต
แก้บั๊ก in-app purchase ทำงานไม่ถูกต้องเมื่อปิดการใช้ cellular data
ปรับปรุงเสถียรภาพ Kids Corner

ที่มา: Windows Experience Blog, MicrosoftInsider.es
