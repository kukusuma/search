https://www.blognone.com/node/7734417:0601/02/16IBM ประกาศเสร็จสิ้นการเข้าซื้อส่วนกิจการของ The Weather Company แล้วhttps://www.blognone.com/sites/default/files/imagecache/news-thumbnail/category_pictures/watson.pngIBM
Watson
nrad6949
หลังจากประกาศว่าจะเข้าซื้อกิจการของ The Weather Company ยกเว้นส่วนโทรทัศน์ไปเมื่อตุลาคมปีที่แล้ว ล่าสุด IBM ก็รายงานว่าการเข้าซื้อนั้นเสร็จสมบูรณ์อย่างเป็นทางการแล้ว
ตามแผนการเข้าซื้อกิจการในครั้งนี้ IBM จะรวมเอาส่วนกิจการที่ซื้อมา เข้าไปยังแผนก Data and Analytics ของบริษัท ซึ่งก็คือแผนก IBM Watson นั่นเอง โดย David Kenny อดีตซีอีโอของ The Weather Company จะได้รับตำแหน่งผู้จัดการทั่วไปของแผนก IBM Watson ส่วน Mike Rhodin จะถูกขยับให้ไปดำรงตำแหน่งรองประธานบริหารฝ่ายพัฒนาธุรกิจของ IBM Watson แทน
ที่มา - Fortune
