https://www.blognone.com/node/7747723:0603/02/16การกลับมาของดรีมทีม? Hideo Kojima เตรียมขึ้นเวทีพร้อมผู้กำกับ Guillermo del Torohttps://www.blognone.com/sites/default/files/imagecache/news-thumbnail/category_pictures/kojima.jpgGames
Hideo Kojima
Konami
mk
สองสุดยอดผู้กำกับข้ามสื่อ Hideo Kojima ผู้สร้าง Metal Gear และ Guillermo del Toro ผู้กำกับภาพยนตร์ชื่อดัง (Pacific Rim) เคยมีแผนทำเกม Silent Hills ร่วมกัน แต่โครงการแท้งไปก่อนเพราะ Hideo Kojima มีปัญหากับ Konami
แต่เมื่อ Kojima ปลดล็อคตัวเองจาก Konami ได้แล้ว การแท็กทีมของคู่หูคู่นี้กำลังจะกลับมาอีกครั้ง โดยทั้งคู่จะขึ้นเวทีงาน D.I.C.E 2016 (ย่อมาจาก Design, Innovate, Communicate, Entertain) ที่ลาสเวกัสระหว่างวันที่ 16-18 กุมภาพันธ์นี้
หัวข้อการสนทนาใช้ชื่อว่า A Conversation with Hideo Kojima and Guillermo del Toro ต้องรอดูว่าเราจะได้เห็นความคืบหน้าอะไรบ้างจากทั้งคู่ครับ
ที่มา - Polygon

