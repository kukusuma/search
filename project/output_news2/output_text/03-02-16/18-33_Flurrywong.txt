https://www.blognone.com/node/7746818:3303/02/16Qualcomm จับมือกับรัฐบาลมณฑลกุ้ยโจว ลุยตลาดชิป ARM สำหรับเซิร์ฟเวอร์ในจีนhttps://www.blognone.com/sites/default/files/imagecache/news-thumbnail/category_pictures/qualcomm.pngARM
China
Qualcomm
Server
Flurrywong
เดือนตุลาคมที่ผ่านมา Qualcomm เปิดเผยว่ากำลังออกแบบชิป ARM เพื่อใช้ในเซิร์ฟเวอร์ตามข่าวเก่า
ความคืบหน้าล่าสุดคือเมื่อวันอาทิตย์ที่ผ่านมา Qualcomm ประกาศความร่วมมือกับรัฐบาลมณฑลกุ้ยโจวของจีน ก่อตั้งบริษัทร่วมค้า Guizhou Huaxintong Semi-Conductor Technology ขึ้น บริษัทนี้จะเน้นการออกแบบ การพัฒนา และการขายชิปสำหรับเซิร์ฟเวอร์ประสิทธิภาพสูงในจีน
Qualcomm ให้เหตุผลของการเปิดบริษัทร่วมค้าว่าจีนเป็นตลาดเทคโนโลยีเซิร์ฟเวอร์เพื่อการพาณิชย์ที่มีขนาดใหญ่เป็นอันดับสองของโลก และ Qualcomm จะเปิดบริษัทลงทุนในกุ้ยโจว เพื่อนำร่องในการลงทุนในจีนของ Qualcomm ในอนาคตอีกด้วย
บริษัทร่วมค้า Guizhou Huaxintong Semi-Conductor Technology จดทะเบียนที่เขตเมืองใหม่ของกุ้ยอัน ซึ่งเป็นเมืองเอกของมณฑลกุ้ยโจว แต่ดำเนินกิจการในปักกิ่ง มีทุนจดทะเบียนที่ 240 ล้านเหรียญสหรัฐ สัดส่วนการถือหุ้นคือทาง Qualcomm ร้อยละ 45 และทางรัฐบาลมณฑลกุ้ยโจวร้อยละ 55
ที่มา - The Register, Computer World Hong Kong
