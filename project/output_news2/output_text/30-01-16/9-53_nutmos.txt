https://www.blognone.com/node/772829:5330/01/16Google เตรียมเปิดตัวบริการโทรศัพท์ผ่าน Google Fiberhttps://www.blognone.com/sites/default/files/imagecache/news-thumbnail/category_pictures/fiber-m.pngGoogle
Google Fiber
nutmos
สำนักข่าวต่างประเทศรายงานว่า Google กำลังเตรียมตัวเปิดบริการโทรศัพท์กับ Google Fiber หรือที่ตอนนี้เรียกกันว่า Google Fiber Phone โดยตอนนี้ Google เริ่มส่งคำเชิญไปถึงผู้ใช้งาน Google Fiber ที่เป็นสมาชิกในโครงการ Trusted Tester (กลุ่มผู้ที่พร้อมจะทดลองบริการใหม่ๆ) ให้ทดลองใช้งานเป็นกลุ่มแรก
Google Fiber Phone จะเป็นบริการที่คล้ายๆ Google Voice และคาดว่าน่าจะมีฟีเจอร์ต่างๆ ของ Google Voice มาด้วยอย่าง การถอดความข้อความเสียง (voicemail transcription), การสกรีนการโทรศัพท์ตามช่วงเวลาในแต่ละวัน
ถ้า Google Fiber จะเปิดบริการโทรศัพท์จริง เท่ากับว่าบริการผ่านสายไฟเบอร์ของ Google จะรวม 3 บริการได้เลยคือโทรศัพท์, โทรทัศน์ และอินเทอร์เน็ตความเร็วสูง
ที่มา - The Washington Post
