https://www.blognone.com/node/771918:0128/01/16Windows Insider จะสามารถอัพเดตเฟิร์มแวร์บน Windows 10 Mobile ได้ในอนาคตhttps://www.blognone.com/sites/default/files/imagecache/news-thumbnail/category_pictures/w10m.pngFirmware
Microsoft
Windows 10 Mobile
nuntawat
สืบเนื่องจากข่าว แอพ Windows Insider มีอัพเดต สามารถรับอัพเดตเป็นทางการได้โดยไม่ต้องรีเซ็ตเครื่องแล้ว ล่าสุดไมโครซอฟท์เผยว่าอีกไม่กี่สัปดาห์ข้างหน้าจะสามารถปล่อยเฟิร์มแวร์อัพเดตให้มือถือที่เข้าร่วมโครงการ Windows Insider ได้
ไมโครซอฟท์ยังทำ workaround ชั่วคราวให้ Windows Insider ที่ต้องการอัพเดตเฟิร์มแวร์ด้วย โดยเข้าไปที่แอพ Windows Insider คลิก Get preview builds แล้วเลือก Production เพื่อหยุดเข้าร่วมโครงการ Windows Insider หลังจากอัพเดตเฟิร์มแวร์แล้วค่อยเข้าร่วมโครงการใหม่
ที่มา: Microsoft Community
