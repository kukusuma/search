https://www.blognone.com/node/774858:2004/02/16Instagram เริ่มทดสอบระบบล็อกอินหลายบัญชีบน iOShttps://www.blognone.com/sites/default/files/imagecache/news-thumbnail/category_pictures/instagram-new.pngInstagram
nutmos
ก่อนหน้านี้ Instagram ได้เริ่มทดสอบระบบล็อกอินหลายบัญชีบนแอพมือถือ ซึ่งตอนนั้นทดสอบเฉพาะ Android เท่านั้น แต่ตอนนี้ Instagram ได้ทดสอบแอพบน iPhone ให้รองรับระบบล็อกอินหลายบัญชีแล้ว
เช่นเดิมคือ ตอนนี้ Instagram ยังคงเปิดฟีเจอร์นี้ให้ทดสอบเฉพาะผู้ใช้กลุ่มเล็กๆ เท่านั้น ซึ่งการล็อกอินหลายบัญชีน่าจะทำให้คนที่มี Instagram หลายบัญชีจะได้ไม่ต้องล็อกเอาท์และล็อกอินใหม่อีกรอบ สามารถแตะเลือกบัญชีกันได้เลย
ที่มา - The Next Web, Latergramme

