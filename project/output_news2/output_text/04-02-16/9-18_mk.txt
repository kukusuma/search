https://www.blognone.com/node/774929:1804/02/16ไมโครซอฟท์เสนอให้ลูกค้า Parse ย้ายระบบหลังบ้านแอพมาใช้ Azure App Servicehttps://www.blognone.com/sites/default/files/imagecache/news-thumbnail/category_pictures/ms-azure.pngDevelopment
Microsoft Azure
MongoDB
Parse
mk
จากข่าว Parse ปิดตัว นักพัฒนาแอพที่ใช้ระบบหลังบ้านของ Parse จำเป็นต้องย้ายหาที่อยู่ใหม่
คู่แข่งอย่างไมโครซอฟท์ที่มีบริการแบบเดียวกัน Azure App Service (ชื่อเดิมคือ Azure Mobile Services) ย่อมไม่พลาดโอกาสนี้ และชักชวนให้ลูกค้า Parse ย้ายมาใช้ระบบของตัวเองแทน
Azure App Service ใช้ระบบหลังบ้านที่เขียนด้วย Node.js เหมือนกับ Parse การย้ายจึงไม่ยากนัก สิ่งที่ขาดหายไปคือระบบส่งข้อความพุช ซึ่งไมโครซอฟท์ก็แนะนำให้ย้ายมาใช้ Azure Notification Hubs แทน
ประเด็นเรื่องฐานข้อมูลของ Parse ใช้ MongoDB ที่ลูกค้า Azure สามารถติดตั้งเองบน VM แต่ไมโครซอฟท์ก็เสนอฐานข้อมูล DocumentDB เป็นทางเลือกด้วย
ที่มา - Azure Blog

