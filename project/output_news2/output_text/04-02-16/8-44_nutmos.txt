https://www.blognone.com/node/774888:4404/02/16Instagram เริ่มขยายความยาวโฆษณาจาก 30 วินาทีเป็น 60 วินาทีhttps://www.blognone.com/sites/default/files/imagecache/news-thumbnail/category_pictures/instagram-new.pngInstagram
nutmos
วันนี้ Instagram ได้เริ่มทดสอบโฆษณา 60 วินาทีเป็นครั้งแรก จากเดิมที่จำกัดโฆษณาไว้เพียง 30 วินาที โดยเริ่มจากโฆษณาของ T-Mobile และ Warner Brothers ซึ่ง Instagram ให้เหตุผลว่านักโฆษณามีสิ่งที่อยากแสดงมากมาย และต้องการทางเลือกมากขึ้นเพื่อให้บรรลุเป้าหมายทางธุรกิจ
สองโฆษณาที่ได้รับการขยายเป็น 60 วินาที คือโฆษณาของ T-Mobile ที่นำโฆษณาความยาว 30 วินาทีที่ฉายในช่วง Super Bowl มาขยายโดยเพิ่มฉากใหม่เข้ามา และ Warner Brothers โฆษณาภาพยนตร์ How To Be Single
การที่ Instagram เพิ่มเวลาโฆษณาน่าจะเป็นหนึ่งในวิธีดึงส่วนแบ่งโฆษณาจากทีวีมาลงใน social network ให้มากขึ้น โดย Instagram จะใช้วิธีค่อยๆ ให้ผู้ใช้ชินกับโฆษณา โดยเริ่มจากโฆษณารูปภาพอย่างเดียว, ตามมาด้วยวิดีโอ 15 วินาที, โฆษณารูปภาพคลิกได้, เพิ่มความยาววิดีโอขึ้นเรื่อยๆ
ที่มา - TechCrunch




 We’re in the #BigGame with @ChampagnePapi. #YouGotCarriered
A video posted by tmobile (@tmobile) on Feb 3, 2016 at 8:37am PST


