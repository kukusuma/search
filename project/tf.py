# -*- coding: cp874 -*-
# -*- coding: utf-8 -*-
import glob
import math
import  csv,codecs
import os
import unicodecsv as csv
from datetime import datetime
import time
import re
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn import feature_extraction
from sklearn.cluster import KMeans

#get  all the files from word-tokenize
flist = glob.glob(r'D:\Project\python\search\project\output_news\test\*.txt')
data = []
#store list word token all doc
list_word = []
##spacial_char = ['',' ', '.', '%','+','*','#','?']
spacial_char =['',' ', '.', '%','+','*',"�", "�", "(", ")", "\"", "#", ",", "@", "-", "/", "[", "]", "{", "}", "!", "?", "$", "~", "^", "&", "'", ",", ":",";"]
name_list = []
#split word
def token(doc):
    #get file name
    str_line = "Term"+','
    i = 1
    for fname in flist:
        name = os.path.basename(fname).replace('.txt','')
        name_list.append(name)
        #str_line+='D'+str(i)+','
        word = []
        tfile = open(fname,"r")
        # read the content of file and store in "line"
        line = tfile.read()
        tfile.close()
        line = line.split('\n')
        for l in line:
            if l in spacial_char:
                continue
            word.append(l.decode('utf-8'))
        #add word token in list_word
        list_word.append(word)
        i+=1
    #data.append(str_line)
    return list_word

#calculate inverse doc freq
def idf(token_word):
    #store idf value for each term
    idf_value = {}
    set_all_word = set()
    #all term in all doc
    set_all_word = set_all_word.union(set(item for sublist in token_word for item in sublist))
    set_all_word = sorted(set_all_word)
    for tkn in set_all_word:
        #find word in all doc �Ҥӷ��ç�Ѻtkn������͡���������df
        doc_feq  = map(lambda doc: tkn in doc, token_word)
        #store idf value use tkn is key �ӹǳ���idf�ͧ���Ф�
        #len(token_word) = number of document
        #sum(doc_feq) = number doc is find word(tkn)
        idf_value[tkn] = math.log10(len(token_word)/(sum(doc_feq)))
    return idf_value

#calculate tf-idf
def tfidf(all_doc):
    idf_value = {}
    tfidf_all_doc = []
    token_word = token(all_doc)
    idf_all_word = idf(token_word)
    i = 1
    for doc in token_word:
        tfidf_doc = []
        for term in sorted(idf_all_word.keys()):
            tf = doc.count(term)
            tf_idf = tf * idf_all_word[term]
            tfidf_doc.append(tf_idf)
            
        tfidf_all_doc.append(tfidf_doc)
        i+=1
    
    str_line = 'Doc'+','
    ct = len(idf_all_word.keys())-1
    
    for term in sorted(idf_all_word.keys()):
        str_line += term
        if ct>0:
            str_line+=','
        ct-=1
    data.append(str_line)
    str_line = ''
    i = 1
    n = 0
    for tfidf_doc in tfidf_all_doc:
        str_line = name_list[n]+','
        cl = len(tfidf_doc)-1
        for tf in tfidf_doc:
            str_line+=str(tf)
            if cl >0:
                str_line+=','
                cl-=1
            #print str_line
             
            i+=1
        n+=1
        data.append(str_line)
    
##    l = 0
##    for term in idf_all_word.keys():
##        print term
##        str_line = term+','
##        
##        ct = len(tfidf_all_doc)-1
##        for i in tfidf_all_doc:
##            str_line+=str(round(i[l],3))
##            if ct >0:
##                str_line+=','
##            ct-=1
##        l+=1
##
##        data.append(str_line)
   
    path="D:\Project\python\search\project\output_news\\"
    fullpath = os.path.join(path,"test-weka-t.txt")
    with open(fullpath, "w") as txt_file:
        for line in data:
##            print line
            txt_file.write(line.encode('utf-8'))
            txt_file.write('\n')
            
    
    '''  
    path="D:\\Project\\python\\search\\project\\output_news\\table1.csv"
    
    with open(path, "wb") as csv_file:
        writer = csv.writer(csv_file, delimiter=',')
        for line in data:
            writer.writerow(line)
    '''
    

    return tfidf_all_doc



def skl_cluster(tfidf_matrix):
    num_clusters = 2
    #km = KMeans(n_clusters=num_clusters)
    km = KMeans(n_clusters=num_clusters, init='k-means++', max_iter=100, n_init=1)
   
    myarray = np.asarray(tfidf_matrix)
    km.fit(tfidf_matrix)
    clusters = km.labels_.tolist()
    docc = {'f':flist,'cluster':clusters}
    frame = pd.DataFrame(docc, index = [clusters] , columns = ['f','cluster'])
    cluster_colors = {0: '#1b9e77', 1: '#d95f02', 2: '#7570b3', 3: '#e7298a', 4: '#66a61e'}
    print type(frame.clusters)
    doc = []
    #print type(doc)
    i = 1
    for d in flist:
        doc.append(i)
        i+=1
    print doc
    plt.scatter(myarray[:, 0], myarray[:, 1],c =doc )
    #plt.scatter(myarray)
    plt.show()
    #plt.scatter(km.cluster_centers_[:, 0], km.cluster_centers_[:, 1], marker='o', s=20)
    #plt.show()
    #print tff print(km.cluster_centers_)
    
    clusters = km.labels_.tolist()
   
    #print km
    #print frame['cluster'].value_counts()
    #return clusters
        
def main():
        value = tfidf(flist)
    
        '''
        myarray = np.asarray(value)
        print myarray
        kmeans = KMeans(n_clusters = 3)
        kmeans.fit(myarray)
        cen = kmeans.cluster_centers_
        labels = kmeans.labels_
        colors = ["g.","r."]
        
        for i in range(len(myarray)):
            #print ("coor :"+myarray[i]+"label:"+labels[i])
        
            plt.plot(myarray[i][0],myarray[i][1],colors[labels[i]],markersize = 10)
        plt.scatter(cen[:, 0],cen[:, 1], marker='o', s=20)
            
        plt.show()
        '''
##        skl_cluster(value)
        
        
        end_time = datetime.now()
        print'Duration: ',format(end_time - start_time)
start_time = datetime.now()
if __name__ == "__main__":
    main()
