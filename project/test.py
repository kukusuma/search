# -*- coding: cp874 -*-
import glob
import math
import csv
import sys
import codecs
import os
from sklearn.feature_extraction.text import TfidfVectorizer,CountVectorizer,TfidfTransformer
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from sklearn import decomposition
from sklearn.externals import joblib
#from __future__ import print_function
from sklearn.metrics.pairwise import cosine_similarity
import matplotlib as mpl
from sklearn.manifold import MDS
from sklearn.decomposition import RandomizedPCA
from sklearn.decomposition import PCA
from sklearn.pipeline import Pipeline
from shutil import copy
line=''
s=set()
list_word = []
spacial_char = ['',' ', '.', '%','+','*','#']
titles = []
flist=glob.glob(r'D:\Project\python\search\project\output_news\test\*.txt')
all_doc = []
wo = ""
for fname in flist:
    word = []
    tfile=open(fname,"r")
    name = os.path.basename(fname).replace('.txt','')
    titles.append(name)
    line=tfile.read() # read the content of file and store in "line"
    all_doc.append(line)
    #add word token in list_word
    line = line.split('\n')
    for w in line:
        if w in spacial_char:
            continue
        wo+=w+'\n'
        word.append(w)
    list_word.append(wo)
tfile.close() # close the file

tokenize = lambda doc: doc.lower().split("\n")
sklearn_tfidf = TfidfVectorizer(norm='l2',min_df=0, use_idf=True, smooth_idf=False, sublinear_tf=True, tokenizer=tokenize,stop_words = 'english')
tfidf_matrix= sklearn_tfidf.fit_transform(list_word)
print tfidf_matrix
##for i in  tfidf_matrix:
##    print i
##p = zip(all_doc,X.data)
##
##for i in p:
##    print i
##fna = sklearn_tfidf.get_feature_names()
##de = X.todense()
##ep = de[0].tolist()[0]
##ps = [pair for pair in zip(range(0,len(ep)),ep)if pair[1]>0]
##sp = sorted(ps,key=lambda t:t[1]*-1)
##for ph,sc in[(fna[word_id],sc)for(word_id,sc)in sp][:200]:
##    print('{0:<20}{1}'.format(ph.encode('utf-8'),sc))

X = cosine_similarity(tfidf_matrix)
labels = {0: 'moblie', 1: 'app', 2: 'com', 3: 'game', 4: 'apple',5:"5",6:"6",7:"7",8:"8",9:"9"}
#labels = titles
true_k = np.unique(labels).shape[0]
print("n_samples: %d, n_features: %d" % X.shape)
s,f = X.shape
m = zip(sklearn_tfidf.get_feature_names(),X.data)
print X
##print s
##plt.plot(s,f)
##plt.show()
terms = sklearn_tfidf.get_feature_names()
print len(terms)
colors = ['#1b9e77', '#d95f02', '#7570b3', '#e7298a']
km = KMeans(n_clusters=10, init='k-means++', max_iter=100, n_init=1)
km.fit(X)
order_centroids = km.cluster_centers_
labels = km.labels_

index = 0
path="D:\Project\python\search\project\output_news\\"
fullpath = os.path.join(path,"test-doc.txt")
##with open(fullpath, "w") as txt_file:
##    for i in X:
##        print "--"
##    txt_file.write(X.data)
    
##    for line in data:
##        txt_file.write(line.encode('utf-8'))
##        txt_file.write('\n')


print labels
for i in labels:
   
    path = "D:\Project\python\Search\project\output_news\output_json"
    for root ,dirs,folders in os.walk(path):
        for file in folders:
            #print file
            if titles[index] == file.replace('.json',''):
                fullpath_out = os.path.join(root,file)
##                print fullpath_out
                path_new = "D:\Project\python\Search\project\output_news\cluster-test-cos\\"+str(i)
                if not os.path.exists(path_new):
                    os.makedirs(path_new)
                fullpath_in = os.path.join(path_new,file)
##                print fullpath_in
                copy(fullpath_out,fullpath_in)
    index+=1
##
km = KMeans(n_clusters=10, init='k-means++', max_iter=100, n_init=1)
km.fit(tfidf_matrix)
order_centroids = km.cluster_centers_
labels = km.labels_
index = 0
print labels
for i in labels:
    path = "D:\Project\python\Search\project\output_news\output_json"
    for root ,dirs,folders in os.walk(path):
        for file in folders:
            #print file
            if titles[index] == file.replace('.json',''):
                fullpath_out = os.path.join(root,file)
##                print fullpath_out
                path_new = "D:\Project\python\Search\project\output_news\cluster-test-tf-idf\\"+str(i)
                if not os.path.exists(path_new):
                    os.makedirs(path_new)
                fullpath_in = os.path.join(path_new,file)
##                print fullpath_in
                copy(fullpath_out,fullpath_in)
    index+=1











##mds = MDS(n_components=2, dissimilarity="precomputed", random_state=1)
##pos = mds.fit_transform(X)
##'''
##for i in range(len(X)):
##    print "coordinate:",X[i],"labels:",labels[i]
##    
##    plt.plot(X[i][0],X[i][1],markersize = 10)
##'''
##plt.scatter(order_centroids[:,0],order_centroids[:,1])
##plt.show()
##for i in labels:
##    print("Cluster %d:" % i)
##    for ind in order_centroids[i, :20]:
##        print(' %s' % terms[ind])
##        #plt.plot(terms[ind])
##    print()
    
'''
pipeline = Pipeline([
    ('vect', CountVectorizer()),
    ('tfidf', TfidfTransformer()),
])
X = pipeline.fit_transform(all_doc).todense()

pca = PCA(n_components=2).fit(X)
data2D = pca.transform(X)
plt.scatter(data2D[:,0], data2D[:,1],marker='o', s=50, linewidths=1, c='b')
#plt.show()

kmeans = KMeans(n_clusters=5, init='k-means++', max_iter=100,n_init=1).fit(X)
centers2D = pca.transform(kmeans.cluster_centers_)

#plt.hold(True)
plt.scatter(centers2D[:,0], centers2D[:,1], 
            marker='x', s=200, linewidths=3, c='r')
plt.show()           
'''
