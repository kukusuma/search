# -*- coding: cp874 -*-
import glob
import math
import csv
import sys
import codecs
import os
from sklearn.feature_extraction.text import TfidfVectorizer,CountVectorizer,TfidfTransformer
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from sklearn import decomposition
from sklearn.externals import joblib
#from __future__ import print_function
from sklearn.metrics.pairwise import cosine_similarity
import matplotlib as mpl
from sklearn.manifold import MDS
from sklearn.pipeline import Pipeline
from sklearn.decomposition import PCA
from shutil import copy
line=''
s=set()
list_word = []
spacial_char = ['',' ', '.', '%','+','*',"#","0-9"]
flist=glob.glob(r'D:\Project\python\search\project\output_news\test\*.txt') #get all the files from the d`#open each file >> tokenize the content >> and store it in a set
all_doc = []
titles = []
filename = []
al = ""
wo = ""
for fname in flist:
    word = []
    wo = ""
    tfile=open(fname,"r")
    name = os.path.basename(fname).replace('.txt','')
    
    filename.append(name)
    line=tfile.read() # read the content of file and store in "line"
    all_doc.append(line)
    #add word token in list_word
    line = line.split('\n')
    title = ""
    i = 2
    for w in line:
        if w == "#":
            i-=1
            continue
        if i>0:
            title+=w
        if w in spacial_char:
            continue
        wo+=w+'\n'
        word.append(w)
    titles.append(title)
    list_word.append(wo)
tfile.close() # close the file

'''
for i in titles:
    print i
'''
    
    #line = line.decode('utf-8')
    
    #s=s.union(set(line.split('\n'))) # union of common words

'''
for i in list_word:
    print i
'''
##s=sorted(s) # sorts the content alphabetically
##
##
##i=0
##ct=0
##tf_line=''
##doc_counts=[]
##for term in s: #takes each term in the set
##    doc_counts.append(0)
##
##    for fdoc in flist:
##        # counts the no of times "term" is encountered in each doc
##
##        doc=open(fdoc)
##        line=doc.read()
##        doc.close()
##        ct=line.count(str(term))
##        #counts the no. of times "term" is present in the file
##        tf_line+=str(ct)+','
##        #prints the count in each doc seperated by comma
##        if (ct>0):
##            #counts no of docs in which
##            doc_counts[i]+=1
##            #this "term" is found
##    #print ct
##    i+=1
##    tf_line=tf_line.strip()+'\n'
##
##idf=[]  #inverse document frequency
##weights=[]      #weight
##total_docs=len(flist)   #total number of documents
##
##i=0
##
##for doc_count in doc_counts:
##    #takes the 1st doc count
##    idf.append(math.log10(total_docs/doc_count)) #calculates idf for each "term"
##    weights.append(idf[i]*doc_count) #calculate weight of the term
##    i+=1
##
##
##final = []
##final_line='TERM'+','
##i=1
##for f in flist:
##    final_line+='D'+str(i)+','
##    i+=1
##final_line+='IDF'+','+'TF-IDF'
##final.append(final_line)
###final.append(final_line.split(','))
##
##tf_arr=tf_line.split('\n')

##for p in list_word:
##    print p
##    print len(p)
##    print "------"

tokenize = lambda doc: doc.lower().split("\n")
##sklearn_tfidf = TfidfVectorizer(norm='l2',min_df=0\
##                                , use_idf=True, smooth_idf=False\
##                                , sublinear_tf=True, tokenizer=tokenize)

sklearn_tfidf = TfidfVectorizer( max_df=0.5,min_df=0,max_features=200000,\
                                 stop_words='english',\
                                 use_idf=True, tokenizer=tokenize)

tfidf_matrix = sklearn_tfidf.fit_transform(list_word)
##print tfidf_matrix
terms = sklearn_tfidf.get_feature_names()
##print terms[:250]
##print len(cosine_similarity(tfidf_matrix))
##
dist = 1-cosine_similarity(tfidf_matrix)

##km = KMeans(n_clusters=10, init='k-means++', max_iter=100, n_init=1)
km = KMeans(n_clusters=10)
km.fit(tfidf_matrix)
##km.fit(dist)
clu = km.labels_.tolist()
##print clu
##print tfidf_matrix
#plt.scatter(X[:, 0], X[:, 1])
#plt.scatter(km.cluster_centers_[:, 0], km.cluster_centers_[:, 1], marker='o', s=20)
#plt.show()
#km = joblib.load('doc_cluster.pkl')
clusters = km.labels_.tolist()
docc = {'f':all_doc,'cluster':clusters,'title':titles,'fname':filename}
frame = pd.DataFrame(docc, index = [clusters] , columns = ['f', 'cluster','title','fname'])
##print frame['title'].value_counts()
k_means_labels = km.labels_
k_means_cluster_centers = km.cluster_centers_.argsort()[:, ::-1] 
##print k_means_labels
index = 0
for i in k_means_labels:
    
    path = "D:\Project\python\Search\project\output_news\output_json"

    for root ,dirs,folders in os.walk(path):
        for file in folders:
            #print file
            if filename[index] == file.replace('.json',''):
                fullpath_out = os.path.join(root,file)
##                print fullpath_out
                path_new = "D:\Project\python\Search\project\output_news\cluster-tf_\\"+str(i)
                if not os.path.exists(path_new):
                    os.makedirs(path_new)
                file = file.replace('.json','_')+str(index)+'.json'
                fullpath_in = os.path.join(path_new,file)
##                print fullpath_in
                copy(fullpath_out,fullpath_in)
    index+=1

    


for i in range(10):
    num = 1
    top_word = ""
    top_title = ""
    
    
   
    for ind in k_means_cluster_centers[i, :20]: #replace 6 with n words per cluster
        if num  == 20:
            top_word+=terms[ind]
        else:
            top_word+=terms[ind]+","
        num+=1
    num = 0
    for title in frame.ix[i]['title']:
        
        top_title+=title+"\n"
        num+=1

    print("Cluster %d words:" % i)
    print top_word
    print("Cluster %d titles:" % i)
    print top_title
    print ("Number of Cluster %d  " %num)

colors = {0: '#1b9e77', 1: '#d95f02', 2: '#7570b3', 3: '#e7298a', 4: '#66a61e' ,5:'r',6:'#5e1ea6',7:'b',8:'y',9:'g'}


#set up cluster names using a dict
cluster_names = {0: 'moblie', 
                 1: 'app', 
                 2: 'com', 
                 3: 'game', 
                 4: 'apple',}
        
cluster_names = {0: '0', 
                 1: '1', 
                 2: '2', 
                 3: '3', 
                 4: '4',
                 5:'5',
                 6:'6',
                 7:'7',
                 8:'8',
                 9:'9'}
mds = MDS(n_components=2, dissimilarity="precomputed", random_state=1)

pos = mds.fit_transform(dist)  # shape (n_components, n_samples)

xs, ys = pos[:, 0], pos[:, 1]
df = pd.DataFrame(dict(x=xs, y=ys, label=clusters , fname=filename)) 
groups = df.groupby('label')
fig, ax = plt.subplots(figsize=(17, 9))
ax.margins(0.05)

for name, group in groups:
    ax.plot(group.x, group.y, marker='o', linestyle='', ms=12, 
            label=cluster_names[name], color=colors[name], 
            mec='none')
    ax.set_aspect('auto')
    ax.tick_params(\
        axis= 'x',          # changes apply to the x-axis
        which='both',      # both major and minor ticks are affected
        bottom='off',      # ticks along the bottom edge are off
        top='off',         # ticks along the top edge are off
        labelbottom='off')
    ax.tick_params(\
        axis= 'y',         # changes apply to the y-axis
        which='both',      # both major and minor ticks are affected
        left='off',      # ticks along the bottom edge are off
        top='off',         # ticks along the top edge are off
        labelleft='off')
    
ax.legend(numpoints=1)  #show legend with only 1 point

#add label in x,y position with the title

for i in range(len(df)):
    ax.text(df.ix[i]['x'], df.ix[i]['y'], df.ix[i]['fname'], size=8)  

plt.show()
print "DONE"

