# เปิดตัวซอฟต์แวร์สตอเรจของตัวเองCoreOS Torus #
เปิดตัวซอฟต์แวร์สตอเรจแบบกระจายตัวเปิดให้เซิร์ฟเวอร์เข้ามาเมาน์เป็นไปได้และในอนาคตจะเก็บแบบออปเจกต์ได้ด้วยทางระบุว่าระบบสตอเรจเป็นปัญหาใหญ่ของแอปพลิเคชั่นทุกวันนี้มักทำงานแบบกระจายตัวกันและทางทีมงานก็อาศัยประสบการณ์จากการพัฒนามาใช้ในการพัฒนาโดยตัวเองก็ยังใช้งานเก็บของไฟล์ใน
ตอนนี้โครงการยังอยู่ในขั้นเริ่มต้นแต่ก็สามารถใช้งานได้จริงแล้วถ้าใครอยากทดลองสามารถเข้าไปดาวน์โหลดได้จากที่มา
CoreOS Torus block device CoreOS etcd Torus etcd metadata Torus Github CoreOS 