# -*- coding: utf-8 -*-
# -*- coding: cp874 -*-
import os
from datetime import datetime
import time
import re
from subprocess import *
from collections import Counter
import json
import glob
#get pathfile
def getpath():
    path = []
    opath=""
    for root ,dirs,folders in os.walk("D:\Project\python\Search\project\output_news\output_text"):
        for file in folders:
            if file.endswith(".txt"):
                fullpath = os.path.join(root,file)
               
                opath = wordbreak(fullpath)
    writetext(opath)

def clean():
    
    for root ,dirs,folders in os.walk("D:\Project\python\Search\project\output_news\output_json"):
        
        sent = ""
        for file in folders:
            #list word english in title
            list_eng_title = []
            #list word english in content
            list_eng_content = []

            #get file json
            if file.endswith(".json"):
                fullpath = os.path.join(root,file)
                d = {}
                #get data from json file
                with open(fullpath) as json_file:
                    #print json_file.read()
                    d =   json.loads(json_file.read().decode('utf-8'))
                #get title news
                title = d['News'].encode('utf-8')
                #get content
                content = d['Contents'].encode('utf-8')
                #list spacial word
                word_replace = ("ฯ", "ๆ", "(", ")", "\"", "#", ",", "@", "-", "/", "[", "]", "{", "}", "!", "?", "$", "~", "^", "&", "'", ",", ":",";")
                #replace word in titile and content
                for word in word_replace:
                    title = title.replace(word,'')
                    content = content.replace(word,'')

                str_title=[]
                str_content = []
                title =  title.split("\n")
                content = content.split("\n")
                for l in title:
                    str_title+=l.split(" ")
                for l in content:
                    str_content+=l.split(" ")
                #call function findenglish
                list_eng_title = findenglish(str_title)
                list_eng_content = findenglish(str_content)
                
                #call function remove
                str_title = remove(str_title,list_eng_title)
                str_content = remove(str_content,list_eng_content)

                #set path output
                path = "D:\Project\python\search\project\output_news\output_text\\" 
                checkpath(path)

                #set file name
                file_text = file.replace('.json','.txt')
                fullpath = os.path.join(path,file_text)
                #chack filename
                if checkpath(path) == True: 
                    flist=glob.glob(path+'*.txt')
                    for fname in flist:
                        name = os.path.basename(fname).replace('.json','.txt')
                        if name == file_text:
                            break
                
                #open new file
                new_w = open(fullpath,"w")
                lent = 0
                #use "#" is start title
                new_w.write('# ')
                #write title
                for data in str_title:
                    if data in word_replace:
                        continue
                    #plus length string
                    lent+=len(data)

                    #if length moret 1000 write newline
                    if lent >1000:
                        new_w.write("\n")
                        lent = 0
                    #write data
                    new_w.write(data)
                for data in list_eng_title:
                    if data in word_replace:
                        continue
                    new_w.write(data)
                    new_w.write(" ")
                #use "#" is end title
                new_w.write("#\n")

                #write content
                for data in str_content:
                    if data in word_replace:
                        continue
                    #plus length string
                    lent+=len(data)
                    #if length moret 1000 write newline
                    if lent >1000:
                        new_w.write("\n")
                        lent = 0
                    new_w.write(data)
                new_w.write("\n")    
                for data in list_eng_content:
                    #print data
                    if data in word_replace:
                        continue
                    new_w.write(data)
                    new_w.write(" ")     
                new_w.close()

#find english word
def findenglish(data):
    list_eng = []
    for word in data:
         #print type("word")
        if re.match(r'^[a-zA-Z]',word):
            list_eng.append(word)   
    return list_eng
#remove english in data
def remove(data,list_eng):
    for word in list_eng:
        for l in data:
            if l == word:
                data.remove(l)
            if l == "":
                data.remove(l)
            if l == "\n":
                data.remove(l)
            if re.match(r'[0-9].*%*',l):
                data.remove(l)
    return data
#check path
def checkpath(path):
    
    if not os.path.exists(path):
        os.makedirs(path)
    return True

#call swath
def wordbreak(fullpath):
    try:
        filename = os.path.basename(fullpath)
        pathname = os.path.dirname(fullpath)
        ipFile = fullpath
        new_filename = filename.split(".txt")
        opath = "D:\Project\python\Search\project\output_news\output_cut\\"
        checkpath(opath)
        opFile = opath+new_filename[0]+".txt"
        if not os.path.exists(opFile):
            cmd = "swath -b \"|\" -m long -u u,u<%s> %s" %(ipFile, opFile)
            os.popen(cmd)
            
    except ValueError as e:
        print e.value
        
#call stopword&writefile                     
def writetext(path):
    path = "D:\Project\python\Search\project\output_news\output_cut\\"
   
    for file in os.listdir(path):
        #print path
        if file.endswith(".txt"):
            title = []
            content = []
            #remove whitepace /n
            fullpath = os.path.join(path,file)
            data = open(fullpath,'r')
            words = [word.strip() for word in data]
            data.close()

            #write data strip file
            data = open(fullpath,'w')
            for w in words:
                data.write(w)
            data.close()

            #read file splitword
            data = open(fullpath,'r')
            d = data.read()
            split = d.decode('utf-8').split("|")

            #call fn stopword
            text = stopword(split)
            i = 0
            #separate text [titile] [content]
            for t in text:
                if i<=1:
                    if t == '#':
                        i+=1
                    title.append(t)
                else :
                    content.append(t)
                        
            text_title = checkword(title)
            text_content = checkword(content)
            text_data = text_title+text_content
            data.close()

            #write data
            path_write = "D:\Project\python\Search\project\output_news\output_word\\"
            checkpath(path_write)
            fullpath_write = os.path.join(path_write,file)
            
            data = open(fullpath_write,'w')
            
            for t in text_data: 
                t = (t.encode('utf-8'))
                data.write(t)
                data.write('\n')
            data.close
            
    return True

#call stopwordfile
def stopword(text):

    #open file stopword
    read_data = open('D:\Project\python\search\project\swath\stopwords_th.txt','r')
    data = read_data.read()
    data = data.decode('utf-8').split("\n")
    txt = text
    num = 0
    for word in data:
        for t in text:
            if t == word:
                text.remove(t) 
    read_data.close()
    return text

def checkword(text):
    read_data = open('named-entity.txt','r')
    read_word = open('word-th.txt','r')
    data = read_data.read()
    data2 = read_word.read()
    data = data.decode('utf-8').split("\n")
    data2 = data2.decode('utf-8').split("\n")
    data = data+data2
    i = 0
    new_data = []
    del_data = []
    
    
    for d in data:
        if d.find('\ufeff'):
            d =  d.replace(u'\ufeff', '')
        test = d[0]
        index = 0
        for t in text:
           
            del_s = []
            if index>len(text)-1:
                break
            inl = index
            t = text[inl]
            pt = ""
            if t.startswith(test):
                i = index+1
                pt +=t
                del_s.append(t)
                while len(d)>len(pt):
                    if i > len(text)-1:
                        break
                    pt+=text[i]
                    del_s.append(text[i])
                    if pt==d:
                        index = i+1
                        if len(new_data)>0:
                            patt = new_data[-1]
                            if re.match(r'^'+patt+'.+',pt, re.IGNORECASE):
                                new_data.pop(len(new_data)-1)

                        new_data.append(pt)
                        del_data = del_data+del_s
                        #text.remove(text[inl])
                        break
                    i+=1
            if index is inl:
                index+=1
   
    for d in del_data:
        #t = t.encode('utf-8')
        for t in text:
            #print t
            #d = d.encode('utf-8')
            if d==t:
                text.remove(t)
            if t== u'':
                text.remove(u'')
    if '#' in text:
        for n in new_data:
            text.insert(1,n)
        return text
    else:    
        new_data = new_data+text
    
    return new_data




def main():
        clean()
        getpath()
        #checkword(['ท','รู','สวย','ดีแทค','มโน','แมค'])
        end_time = datetime.now()
        print'Duration: ',format(end_time - start_time)
start_time = datetime.now()
if __name__ == "__main__":
    main()
#t = difflib.get_close_matches('ลอกเอ้าท', ['ล็อคเอาท์', 'ล็อกเอาท์', 'ล็อกอิน', 'ล็อคเอ้าท์'])
#find similar word
    
