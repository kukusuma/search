ELLOคอมพิวเตอร์DIYที่ผู้ซื้อต้องนำไปประกอบเองใช้ชิปPIC32
ELLOคือคอมพิวเตอร์ที่อาจไม่เหมาะกับผู้ใช้ทั่วไปแต่เหมาะสำหรับโปรแกรมเมอร์ผู้ต้องการเรียนรู้ระบบและแฮกกิ้งโดยเฉพาะเพราะELLOไม่ใช่คอมพิวเตอร์สำเร็จรูปแต่เป็นคอมพิวเตอร์DIYที่ผู้ใช้งานต้องนำไปประกอบเข้าด้วยกันโดยEllOมาพร้อมกับแผงวงจรไฟฟ้า6ชิ้นความหนารวม6.4มิลลิเมตรบอร์ดทดสอบอุปกรณ์จอLCDTouchscreenกว้าง7นิ้วและแป้นพิมพ์
จุดแข็งของELLOคือเหมาะมากสำหรับผู้เริ่มต้นสร้างโปรแกรมพื้นฐานนอกจากนี้โปรแกรมที่ผู้ใช้งานสร้างขึ้นยังสามารถเชื่อมเข้ากับระบบอิเล็กทรอนิกส์ที่บอร์ดอุปกรณ์ทดสอบแบบไม่ต้องบัดกรีได้ด้วยบอร์ดลงอุปกรณ์ด้านซ้ายบนของคอมพิวเตอร์มีรูถึง1156รูเพื่อรองรับการดัดแปลงของผู้ใช้งานแต่ละคน
ELLOมีซอฟต์แวร์ที่กินสเปกน้อยฮาร์ดแวร์ไม่ใหญ่มากบรรจุ32bitPIC32microcontrollerRAM128kBโมดูล2.4GHzRFแบตเตอรี่4500มิลลิแอมป์
สามารถสั่งซื้อELLOได้ที่crowdsupply.comเป็นเว็บไซต์crowdfundingราคาเต็มสำหรับอุปกรณ์ครบทุกอย่างคือ240เหรียญประมาณ9พันบาทถ้าไม่รวมบอร์ดอุปกรณ์ทดสอบราคาอยู่ที่180เหรียญประมาณ6พันบาทและถ้าซื้อแค่บอร์ดเปล่าราคาอยู่ที่65เหรียญประมาณ2300บาท

ภาพจากcrowdsupply.com
ที่มาGeek
