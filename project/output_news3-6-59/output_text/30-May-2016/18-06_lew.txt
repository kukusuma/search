systemdจะครองโลกปิดโปรแกรมทุกตัวหลังล็อกเอาต์ทำให้screenและtmuxพัง
systemdรุ่น230มีการปรับค่าคอนฟิกเริ่มต้นKillUserProcessesเป็นyesทำให้systemdปิดโปรแกรมทุกตัวทันทีหลังจากผู้ใช้ล็อกเอาต์จากระบบรวมถึงการเชื่อมต่อsshหลุดไปส่งปัญหาต่อโปรแกรมที่เปิดค้างเอาเพื่อป้องกันการเชื่อมต่อหลุดไปอย่างtmuxscreenและx2go
keszybzนักพัฒนาของsystemdระบุว่าทางแก้นั้นทำได้โดยผู้ใช้อาจจะสั่งแก้scopeของการรันด้วยคำสั่งsystemdrunscopeusertmuxทำให้แม้ว่าผู้ใช้หลุดออกจากระบบไปโปรแกรมก็จะรันต่อแต่หากโปรแกรมเช่นtmuxและscreenจะสามารถรันตัวเองโดยเปลี่ยนscopeได้อันโนมัติก็จะดีกว่าและเขาเสนอให้tmuxรองรับการเปลี่ยนscopeด้วยตัวเอง
ข้อเสียของแนวทางนี้คือtmuxจะต้องลากเอาไลบรารีdbusเข้ามาเป็นdependencyด้วยทางnicmนักพัฒนาของdbusระบุว่าฟังก์ชั่นdaemonทำงานได้ค่อนข้างตรงกันทุกระบบอยู่แล้วมานานถึงสามสิบปีทำไมsystemdจึงเลือกมาเปลี่ยนพฤติกรรมและให้แอปพลิเคชั่นรองรับความเปลี่ยนแปลงนี้ไว้เอง
ทางฝั่งsystemdระบุว่าการปิดโปรแกรมทั้งหมดหลังผู้ใช้หลุดออกจากระบบเป็นค่าเริ่มต้นหลังจากมีปัญหาโปรแกรมGUIรันค้างไว้แม้ผู้ใช้จะล็อกเอาต์ไปแล้วก็ตามจึงต้องเปลี่ยนพฤติกรรมนี้
ตอนนี้ทางฝั่งtmuxปฎิเสธจะดึงdbusเข้ามาและระบุว่าถ้ามีAPIให้ใช้งานง่ายก็จะพิจารณาอีกที
ที่มาGithubtmuxDebianBug825394
