25ปีผ่านไปCapcomเผยชื่อ2ตัวละครในฉากเปิดเกมStreetFighterII
ใครที่ทันเล่นเกมในตำนานอย่างStreetFighterIIคงคุ้นเคยกับฉากเปิดเรื่องที่มีชาย2คนยืนชกกันอยู่ริมถนนก่อนขึ้นโลโก้ของเกมนึกไม่ออกดูวิดีโอประกอบเวลาผ่านมา25ปีในที่สุดCapcomก็เปิดเผยชื่อและข้อมูลของชาย2คนนี้ว่าชื่อMaxและScott
ชายผมดำทรงอะโฟรที่ยืนซ้ายมือชื่อMaxเป็นนักมวยรุ่นเฮฟวี่เวทชาวอเมริกันส่วนScottหนุ่มผมทองก็เป็นอดีตนักมวยชาวอเมริกันเช่นกัน
การเปิดเผยข้อมูลตัวละครครั้งนี้ผ่านบล็อกของCapcomและน่าจะเป็นเรื่องขำมากกว่าการเพิ่มตัวละคร2ตัวนี้เข้าไปอยู่ในDLCของStreetFighterVครับในบล็อกชุดเดียวกันยังวาดคาแรกเตอร์นักมวยBalrogโดยใช้ชื่อMikeตามต้นฉบับภาษาญี่ปุ่นให้ดูด้วย
ที่มาCapcom1Capcom2Polygon

Maxไม่รู้ทรงผมหายไปไหน

Scott

Mikeซึ่งมีต้นแบบจากMikeTysonแต่ถูกเปลี่ยนชื่อในฉบับภาษาอังกฤษเป็นBalrog

