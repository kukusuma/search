ASUSเปิดตัวROGGX800เกมมิ่งโน้ตบุ๊คระบายความร้อนด้วยของเหลวรุ่นอัพเกรด
หลังกำลังเตรียมวางขายROGGX700ล่าสุดASUSได้โชว์GX800ในงานComputexที่ไต้หวันโดยรูปลักษณ์ยังคงเหมือนกับGX700ที่มาพร้อมระบบระบายความร้อนด้วยของเหลวด้านหลัง
สเปคของGX800คาดว่าส่วนใหญ่จะยังคงเหมือนกับGX700ยกเว้นเปลี่ยนมาใช้ซีพียูCorei7แบบปลดล็อคระบุเพียงว่าเป็นรหัสKที่สัญญาณนาฬิกา4.4GHzจีพียูใช้ของNVIDIAรุ่นล่าสุดพร้อมต่อSLIสองใบพร้อมพาวเวอร์ซัพพลายขนาด330Wสองตัวไม่ได้ระบุรายละเอียดอื่น
ที่มาASUS


