เจ้าของไอเดียPhonebloksไม่ปลื้มกับความเปลี่ยนแปลงของProjectAra
จากความคืบหน้าล่าสุดของProjectAraที่เลื่อนออกไปเป็นปี2017และลดความยืดหยุ่นลงโดยการรวมเอาส่วนหลักของโทรศัพท์เช่นหน่วยประมวลผลแบตเตอรี่จอภาพมาใว้ที่โครงหลักของตัวเครื่องข่าวนี้ดูเหมือนจะสร้างความผิดหวังให้หลายคนที่ติดตามโครงการนี้อยู่ไม่น้อยบางคนก็บอกว่าไม่ซื้อแล้ว
ล่าสุดนักออกแบบชาวเนเธอร์แลนด์DaveHakkensผู้ริเริ่มไอเดียPhonebloksโทรศัพท์ปรับแต่งได้ในลักษณะของโมดูลก่อนจะมาเป็นProjectAraก็ออกมาพูดถึงเรื่องนี้ผ่านหน้าเว็บไซต์ของเขา
เดิมทีไอเดียของโทรศัพท์ในลักษณะโมดูลนั้นริเริ่มขึ้นเพื่อพยายามลดขยะอิเล็กทรอนิกส์ลงจากการสิ้นอายุขัยของชิ้นส่วนบางชิ้นคลิปคอนเซปต์ของPhonebloksซึ่งHakkensให้ความเห็นว่าการที่รวมหน่วยประมวลผลเซ็นเซอร์เสาอากาศแบตเตอรี่และจอเข้ามาอยู่ในโครงหลักของเครื่องนี้ทำให้เมื่อจอแตกหรือหน่วยประมวลผลเริ่มล้าหลังเราก็ต้องเปลี่ยนโทรศัพท์ใหม่ทั้งเครื่องอยู่ดีแล้วโมดูลก็กลายเป็นเพียงส่วนเสริมสำหรับปรับแต่งเพื่อความสนุกเท่านั้น
นอกจากนี้Hakkensยังพูดถึงเป้าหมายเดิมของโครงการที่จะสร้างโทรศัพท์เพื่อทั้งโลกโดยสร้างมาตรฐานแบบเปิดขึ้นมาเพื่อให้นักพัฒนาสามารถสร้างโมดูลขึ้นมาขายเองได้แต่กลับกลายเป็นทุกอย่างอยู่ภายใต้กูเกิลซึ่งสิ่งที่จะเป็นมาตรฐานแบบเปิดนี้ไม่ควรอยู่ในมือของบริษัทใดบริษัทหนึ่งเพราะจะทำให้เกิดการแข่งขันกันมากกว่าความร่วมมือ
เขายังบอกอีกว่าหากกูเกิลยังต้องการสร้างโทรศัพท์เพื่อทั้งโลกจริงกูเกิลควรร่วมมือกับคนอื่นให้มากกว่านี้และอย่ามุ่งแต่จะสร้างโทรศัพท์ใว้ขาย
ที่มาDaveHakkensผ่านTheVerge
