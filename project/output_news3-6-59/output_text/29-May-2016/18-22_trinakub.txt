PointBlankต้านไม่ไหวทีมไทยแพ้บราซิล21เกมคว้ารองแชมป์PBWC2016
จบกันไปแล้วกับรายการแข่งขันPointBlankWorldChallenge2016ที่ประเทศเกาหลีใต้เมื่อวันที่2829พฤษภาคม2559ชิงเงินรางวัลรวมกว่า1800000บาทโดยตัวแทนทีมชาติไทยทีมSignature.PBแพ้ให้กับทีม2KillGamingจากประเทศบราซิลในรอบชิงชนะเลิศด้วยสกอร์21คว้ารองแชมป์กลับบ้าน
ตัวแทนทีมชาติไทยSignature.PBเป็นตัวแทนไปแข่งที่รายการPBWC2016ที่ประเทศเกาหลีใต้โดยทีมSignature.PBสามารถเอาชนะทีมAoeXeรัสเซียและCrewEsportGamingตุรกีก่อนเข้ารอบชิงชนะเลิศและแพ้ให้กับทีม2KillGamingบราซิลไปด้วยสกอร์21

ผลการแข่งขันรอบแบ่งกลุ่มGroupLeagueวันที่28พ.ค.59
รอบแบ่งกลุ่มแบ่งเป็น2กลุ่มกลุ่มละ3ทีมโดยแข่งแบบพบกันหมด1เกมBO1หาทีมที่มีคะแนนดีที่สุดในกลุ่ม2อันดับแรกผ่านเข้าสู่รอบSemifinal
GroupA

130013402KillGamingBrazilแพ้CreweSportsTurkeyMapDowntownสกอร์67win
13.4014.202KillGamingBrazilชนะOnemoreAviiIndonesiaMapStromtubeสกอร์103win
14.2015.00CreweSportsTurkeyแพ้OnemoreAviiIndonesiaMapSandStromสกอร์68win

GroupB

15.0015.40AoeXeRussiaชนะOffersKoreaMapMidtownสกอร์101win
15.4016.20AoeXeRussiaแพ้Signature.PBThailandMapDowntownสกอร์89win
16.2017.00OffersKoreaVSSignature.PBThailandMapStromtubeสกอร์010win

ผลการแข่งขันวันที่29พ.ค.59
รอบSemiFinalหรือรอบKnockoutจะแข่งแบบแพ้คัดออก2ใน3เกมBO3หาผู้ชนะเข้าสู่รอบชิงชนะเลิศ
คู่แรกเวลา10.00น.
2KillGamingบราซิลชนะAoeXeรัสเซีย20เกม

แมตซ์1แผนที่SandStormบราซิลชนะ109
แมตซ์2แผนที่Luxvilleบราซิลชนะ65
แมตซ์3แผนที่Midtown

คู่สองเวลา11.30น.
Signature.PBไทยชนะCrewE_sportsGamingตุรกี

แมตซ์1แผนที่SandStormไทยชนะ85
แมตซ์2แผนที่Midtownไทยชนะ102
แมตซ์3แผนที่Luxville

รอบชิงอันดับ3
AoeXeรัสเซียแพ้CrewE_sportsGamingตุรกี
รอบชิงชนะเลิศFinal
รอบชิงชนะเลิศก็จะแข่งกันแบบ2ใน3เกมBO3เช่นกัน
Signature.PBไทยแพ้2KillGamingบราซิล

แมตซ์1แผนที่Stormtubeไทยชนะ85
แมตซ์2แผนที่Luxvilleไทยแพ้68
แมตซ์3แผนที่Midtownไทยแพ้38

ที่มาPoitnBlankThailandFanpage
