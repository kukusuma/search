GoogleจับมือTripAdvisorบริการเพลงในธีมท้องถิ่นผ่านPlayMusicยามไปเที่ยว
Googleประกาศความเป็นพันธมิตรกับTripAdvisorในการบริการเพลงผ่านGooglePlayMusicในธีมเพลงของสถานที่ที่เราเดินทางไปท่องเที่ยวโดยแถบwidgetของGooglePlayMusicจะปรากฎขึ้นมาหากเราเปิดแอพTripAdvisor
ฟีเจอร์นี้เปิดให้บริการแล้วในกว่า60ประเทศที่มีGooglePlayMusicให้บริการขณะที่ผู้ที่ไม่เคยใช้งานGooglePlayMusicมาก่อนจะได้สิทธิทดลองใช้ฟรี2เดือนหากดาวน์โหลดแอพTripAdvisorมาใช้งาน
ที่มาOfficialAndroidBlog

