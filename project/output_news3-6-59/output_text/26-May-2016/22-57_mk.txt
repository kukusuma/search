Alibabaโดนทางการสหรัฐสอบสวนเรื่องการลงบัญชีขาดทุนของบริษัทลูก
Alibabaเปิดเผยว่ากำลังถูกสอบสวนจากสำนักงานกำกับหลักทรัพย์ของสหรัฐอเมริกาSECเกี่ยวกับการลงข้อมูลทางบัญชีโดยเกี่ยวข้องกับบริษัทลูกในเครือAlibabaแต่ยังไม่มีรายละเอียดอื่นเพิ่มเติม
Alibabaระบุว่ายินดีให้ความร่วมมือกับทางการสหรัฐโดยมอบข้อมูลเกี่ยวกับบริษัทลูกด้านลอจิสติกส์ชื่อCainiaoซึ่งเป็นบริษัทร่วมทุนของAlibabaกับบริษัทลอจิสติกส์หลายรายให้กับSEC
หนังสือพิมพ์TheNewYorkTimesอ้างข้อมูลจากบริษัทวิจัยตลาดPacificSquareResearchว่าAlibabaอาจใช้การถือหุ้นไขว้ที่ซับซ้อนเพื่อไม่ให้ตัวเลขขาดทุนของบริษัทลูกบางรายไปโผล่เป็นตัวเลขขาดทุนของบริษัทแม่ซึ่งอาจมีผลต่อราคาหุ้นในตลาดหลักทรัพย์
ที่มาTheNewYorkTimes

