# -*- coding: utf-8 -*-
import PyICU

def isThai(chr):
    cVal = ord(chr)
    #print cVal
    if(cVal >= 3584 and cVal <= 3711):
        #print cVal
        return True
    return False

def warp(txt):
    #print txt
    bd = PyICU.BreakIterator.createWordInstance(PyICU.Locale("th"))
    bn = PyICU.BreakIterator.createWordInstance(PyICU.Locale("en"))
    bn.setText(txt)
    bd.setText(txt)
    lastPos = bd.first()
    retTxt = ""
    try:
        while(1):
            currentPos = bd.next()
            retTxt += txt[lastPos:currentPos]
            #print bd.next
            #Only thai language evaluated
            if(isThai(txt[currentPos-1])):
                if(currentPos < len(txt)):
                    if(isThai(txt[currentPos])):
                        #This is dummy word seperator
                        retTxt += "|"
            lastPos = currentPos
    except StopIteration:
        pass
        #retTxt = retTxt[:-1]
    return retTxt
