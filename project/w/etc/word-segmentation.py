import requests
from bs4 import BeautifulSoup
import os
import json 
import urllib
from datetime import datetime
num_page = 0
def crawler():
    
    news = dict
    url = "https://www.blognone.com"
    #url = "http://www.google.com"
    #response = requests.get(url)

    #get data from web
    for num_page in range(10):
        # check page
        if num_page==0: 
            #response = requests.get(url)
            response = urllib.urlopen(url)
        else:
            #response = requests.get(url_page)
            response = urllib.urlopen(next_page)
            
        soup = BeautifulSoup(response.read(),"html.parser")
        #get url next page
        urlpage = soup.find_all('li',{'class':'pager-next'})[0].find_all('a')[0].get('href')
        next_page = url+urlpage
        content_store =  soup.find_all('div',{'class':'content-title-box'})

        try:
            for content in content_store:
                #print content
                for link in content.find_all('a'):
                    #print '*********************'
                    #print link.get('href')
                            
                    #get link content
                    link_content = link.get('href')
                    url_link = url+str(link_content)
                    #print url_link
                    response_link = urllib.urlopen(url_link)
                    #print response_link.content
                    soup2 = BeautifulSoup(response_link.read(),'html.parser')
                    #print soup2

                    data_store = soup2.find_all("div",{"class":"content-container"})
                        
                    #print data
                        
                    for data in data_store:

                        #get head news
                        head = data.find_all('h2')[0].text
                        #print d.find_all('div',{'class':'content-title-box'})[0].text
                
                        #get date-time
                        meta_date = data.find_all("span",{"class":"submitted"})
                        meta_date =  meta_date[0].text.split(" ")
                        by = meta_date[1]
                        date = meta_date[4]
                        
                        time = meta_date[5]
                                    
                        #get tags
                        tags = data.find_all("span",{"class":"terms"})[0].text

                        #get content
                        contents = data.find_all_next("div",{"class":"node-content"})[0].text
                
                        #get image
                        image_link = data.find_all('div',{'class':'content'})[0].find_all("img")[0]['src']
                        
                       

                    #add data to dict
                    news = {'News':head,'By':by,'Date':date,'Time':time,'Tags':tags,'Contents':contents,'Link':url_link,'Image-Link':image_link}
                    #wordbreak(news)
                    
                    #set output path
                    setpath = date.replace("/",'-')
                    output_path = "output12/"+setpath
                    path = output_path
                    
                    #check path 
                    if not os.path.exists(path):
                        os.makedirs(path)
                    #set filename
                    setfilename = time.replace(":","-")
                    filename = os.path.join(path,setfilename+'_'+by+'.json')
                    if not os.path.exists(filename):
                        write_data = open(filename,'a')
                        json_string = json.dumps(news,ensure_ascii = False,sort_keys=True,skipkeys=True,indent=10).encode('utf8')
                        json_string.decode('utf8')
                        write_data.write(json_string)
                        write_data.close()
                num_page+=1
                
        except ValueError as e:
                 print e.value
                 
def wordbreak(news):
    ipFile = writetext(news)
    
    opFile = str(num_page)+'_out.txt'
    cmd = "swath -b \"|\" -m long <%s> %s" %(ipFile, opFile)
    f = os.popen(cmd)
    
    return True
                        
def writetext(news):
    filename = 'input.txt'
    writedata = open(filename,'w')
    new = ""
    for n in news:
        writedata.write(news[n].encode('utf-8'))
    writedata.close()
    return filename
def main():
	crawler()
	end_time = datetime.now()
	print'Duration: ',format(end_time - start_time)
    
start_time = datetime.now()
if __name__ == "__main__":
    main()
