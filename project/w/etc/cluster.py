# -*- coding: utf-8 -*-
import requests
from bs4 import BeautifulSoup
import os
import json
import urllib2
#from __future__ import print_function
num_page = 0
news = dict
url = "https://www.blognone.com"
#get data from web
for num_page in range(10):
    #----change url----
    url_page = url+'/node?page='+str(num_page)
    # check url
    if num_page==0: 
        response = requests.get(url)
        #response = urllib2.urlopen(url)
    else:
        response = requests.get(url_page)
        #response = urllib2.urlopen(url_page)
    #get url
    soup = BeautifulSoup(response.content,"html.parser")
    #soup = BeautifulSoup(response.read())
    #content_store =  soup.find_all('div',{'class':'news-info'})
    content_store =  soup.find_all('div',{'class':'content-title-box'})

    #output store
    output_path = "./output_news1/"+str(num_page)
    count = 1
    try:
        for content in content_store:
            #print content
            for link in content.find_all('a'):
                #print '*********************'
                #print link.get('href')
                        
                #get link content
                link_content = link.get('href')
                url_link = url+str(link_content)
                #print url_link
                response_link = requests.get(url_link)
                #print response_link.content
                soup2 = BeautifulSoup(response_link.content,'html.parser')
                #print soup2

                data_store = soup2.find_all("div",{"class":"content-container"})
                    
                #print data
                    
                for data in data_store:

                    #get head news
                    title = data.find_all('h2')[0].text
                    #print d.find_all('div',{'class':'content-title-box'})[0].text
            
                    #get date-time
                    meta_date = data.find_all("span",{"class":"submitted"})
                    meta_date =  meta_date[0].text.split(" ")
                    by = meta_date[1]
                    date = meta_date[4]
                    time = meta_date[5]
                                
                    #get tags
                    tags = data.find_all("span",{"class":"terms"})[0].text

                    #get content
                    contents = data.find_all_next("div",{"class":"node-content"})[0].text

                    #get image
                    image_link = data.find_all('div',{'class':'content'})[0].find_all("img")[0]['src']
                
                   

                #add data to dict
                #news = {'id':"doc"+str(count),'title':title,'author':by,'Date':date,'time':time,'keywords':tags,'content':contents,'url':url_link,'links':image_link}
                news = {'id':"doc"+str(count),'title':title,'author':by,'keywords':tags,'content':contents,'url':url_link,'links':image_link}
                print news['keywords']
                path = output_path
                if not os.path.exists(path):
                    os.makedirs(path) 
                filename = os.path.join(path,'test'+str(count)+'.json')
                write_data = open(filename,'a')
                json_string = json.dumps(news,ensure_ascii = False,sort_keys=True,indent=4).encode('utf8')
                json_string.decode('utf8')
                
                #write_data.write(json_string)
                count+=1
            num_page+=1
            
    except ValueError:
             print "????"
                  

